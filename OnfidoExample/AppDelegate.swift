//
//  AppDelegate.swift
//  OnfidoExample
//
//  Created by Kerem Gunduz on 28.08.2018.
//  Copyright © 2018 Kerem Gunduz. All rights reserved.
//

import UIKit
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width,
                                        height: UIScreen.main.bounds.size.height))

        window?.rootViewController = UINavigationController(rootViewController: ViewController())
        window?.makeKeyAndVisible()
        return true
    }
}
