//
//  ImagePreviewController.swift
//  OnfidoTask
//
//  Created by Kerem Gunduz on 27.08.2018.
//  Copyright © 2018 Kerem Gunduz. All rights reserved.
//

import UIKit

class ImagePreviewController: UIViewController {

    private var viewImage: UIImageView!
    private var image: UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        viewImage = UIImageView.init(image: image)
        viewImage.translatesAutoresizingMaskIntoConstraints = false
        viewImage.contentMode = .scaleAspectFill
        view.addSubview(viewImage)
        view.leftAnchor.constraint(equalTo: viewImage.leftAnchor).isActive = true
        view.rightAnchor.constraint(equalTo: viewImage.rightAnchor).isActive = true
        view.topAnchor.constraint(equalTo: viewImage.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: viewImage.bottomAnchor).isActive = true
    }

    convenience init() {
        self.init(image: nil)
    }

    init(image: UIImage?) {
        self.image = image
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
